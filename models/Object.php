<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 7/1/15
 * Time: 11:46 AM
 */

namespace sagas_ron14\models;


class Object {
    private $_id;
    private $_name;

    public function __construct($id, $name){
        $this->_id = $id;
        $this->_name = $name;
    }

    public function __toString(){
        $array = ['id'=>$this->_id, 'name'=>$this->_name];
        return json_encode($array);
    }
}